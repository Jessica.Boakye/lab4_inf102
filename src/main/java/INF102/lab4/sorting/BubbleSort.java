package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        //midlertidig lagrer elementene
        T temporary;
        //indeksert for løkke
        for(int i = 0; i < list.size(); i++){
            for(int j = 1; j < list.size()-i; j++){
                //større enn element på posisjon j,
                if(list.get(j-1).compareTo(list.get(j)) == 1){
                     temporary = list.get(j-1);
                     //erstatter element på j-1 med element på j (mindre)
                     list.set(j-1, list.get(j));
                    //erstatter element på j med temp(større)
                     list.set(j, temporary);
                }
            }

        }
    }
    
}
