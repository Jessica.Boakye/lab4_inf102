package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        if(listCopy.isEmpty()){
            throw new IllegalArgumentException();

        }
        int listSize = listCopy.size();
        int k = listSize / 2;
        return findingMedian(listCopy, k);
    }    
        

    public <T extends Comparable<T>> T findingMedian(List<T> listCopy, int k){
        //base case
        if(listCopy.size() == 1){
            return listCopy.get(0);
        }

        Random rand = new Random();
        T pivot = listCopy.get(rand.nextInt(listCopy.size())); 
        List<T> smaller = new ArrayList<>();
        List<T> bigger = new ArrayList<>();
        List<T> median = new ArrayList<>();

        /*
         * for løkke som iterer over hvert element t i listen.Copy
         * smaller: mindre enn pivot
         * median: lik pivoten
         * bigger: større enn pivot
         */
        for(T t : listCopy){

            if(t.compareTo(pivot) < 0){
                smaller.add(t);
            }
            else if(t.compareTo(pivot) == 0){
                median.add(t);
            }else{
                bigger.add(t);
            }
        }
        /*
         * denne delen av kodet fikk jeg hjelp av med ChatGpt
         */
        // kth element er mindre enn antall elementer i smaller
        // true: kth element er i listen smaller
        if(k < smaller.size()){
            return findingMedian(smaller, k);
        }
        else if(k < smaller.size() + median.size()){
            return pivot;
        }else{
            return findingMedian(bigger, k - smaller.size()- median.size());
        }
        
    }

}

